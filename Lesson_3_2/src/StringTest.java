/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 26.10.13
 * Time: 14:53
 * To change this template use File | Settings | File Templates.
 */
public class StringTest {
    String str = "world";
    final int iteration = 8000;
    public long stringTest(){
        String s = "";
        System.currentTimeMillis();
        for(int i=0; i<iteration; i++){
            s=s+iteration;
        }
        long timeEnd = System.currentTimeMillis();
        System.out.println("Running time for String - "+timeEnd);
        return timeEnd;
    }
    public long stringBuilderTest(){
        StringBuilder s = new StringBuilder();
        System.currentTimeMillis();
        for(int i=0; i<iteration; i++){
            s.append(str);
        }
        long timeEnd = System.currentTimeMillis();
        System.out.println("Running time for StringBuilder - "+timeEnd);
        return timeEnd;
    }
    public long stringBufferTest(){
        StringBuffer s = new StringBuffer();
        System.currentTimeMillis();
        for(int i=0; i<iteration; i++){
            s.append(str);
        }
        long timeEnd = System.currentTimeMillis();
        System.out.println("Running time for StringBuffer - "+timeEnd);
        return timeEnd;
    }
    public static void main(String[] args){
        StringTest test = new StringTest();
        long str = test.stringTest();
        long strBuffer = test.stringBufferTest();
        long strBuilder = test.stringBuilderTest();
        System.out.println("String - StringBuffer = "+(str-strBuffer));
        System.out.println("StringBuffer - StringBuilder = "+(strBuffer-strBuilder));

    }
}
