import sun.misc.IOUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Utils class that contains useful method to interact with URLConnection
 */
public class ConnectionUtils {

    /**
     * Downloads content for specified URL and returns it as a byte array.
     * Should be used for small files only. Don't use it to download big files it's dangerous.
     *
     * @param url
     * @return
     * @throws java.io.IOException
     */

    public static byte[] getData(URL url) throws IOException {
        URLConnection conn = url.openConnection();
        InputStream stream = null;
        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
        byte[] byteChunk = null;
        try {
            stream = url.openStream();
            byteChunk = new byte[4096];
            int i;
            while ((i = stream.read(byteChunk)) > 0) {
                byteArray.write(byteChunk, 0, i);
            }
        } catch (IOException e) {
            System.err.printf("Failed while reading bytes from %s: %s", url.toExternalForm(), e.getMessage());
            e.printStackTrace();
        } finally {
            if (stream != null) stream.close();
        }
        return byteChunk;
    }
}
