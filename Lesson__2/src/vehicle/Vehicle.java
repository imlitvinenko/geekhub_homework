package vehicle;
/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 19.10.13
 * Time: 20:55
 * To change this template use File | Settings | File Templates.
 */
public abstract class  Vehicle implements Driveable {
    public abstract int accelerate(Boolean force);
    public abstract void brake();
    @Override
    public String turn(String side){
        return "Повернути агрегат в "+side;
    }
}
