package vehicle;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 19.10.13
 * Time: 22:11
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String []args){
        Driveable vehicle=null;
        Scanner scan = new Scanner(System.in);
        scan = new Scanner(System.in);
        System.out.println("Перевірем як працюють транспортні засоби?");
        System.out.println("Ви пересуваєтесь по дорозі чи по водичці?(input - road/water)");
        String typeCoating  = scan.nextLine();
        System.out.println("На чому двигун працює?(input - disel/gas)");
        String typeFuel  = scan.nextLine();
        System.out.println("Введіть літраж заправки: ");
        int fuel  = Integer.parseInt(scan.nextLine());
        System.out.println("Введіть дистанцію: ");
        int distance  = Integer.parseInt(scan.nextLine());

        if(typeCoating.equals("road") && typeFuel.equals("disel")){
            vehicle = new SolarPoweredCar(fuel,distance);
        }
        else if (typeCoating.equals("road") && typeFuel.equals("gas")) {
            vehicle = new PetrolCar(fuel,distance);
        }
        else if (typeCoating.equals("water")) {
            vehicle = new Boat(fuel,distance);
        }
        if (vehicle==null || fuel<0 || distance<1){
            System.out.println("Збирись! Подумай! Задай правильну інфу! Рушай");
        }


    }
}
