import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 03.11.13
 * Time: 0:39
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] argv) {
        SetClassOperation setClass = new SetClassOperation();
        Set<Integer> multiplicity1 = new HashSet<Integer>();
        Set<Integer> multiplicity2 = new HashSet<Integer>();
        int randNum = 1 + setClass.rand();
        for (int i = 0; i < randNum; i++) {
            multiplicity1.add(setClass.rand());
        }
        System.out.println("multiplicity1: " + multiplicity1);
        randNum = 1 + setClass.rand();
        for (int i = 0; i < randNum; i++) {
            multiplicity2.add(setClass.rand());
        }
        System.out.println("multiplicity2: " + multiplicity2);
        System.out.println("Equals: " + setClass.equals(multiplicity1, multiplicity2));
        System.out.println("Union: " + setClass.union(multiplicity1, multiplicity2));
        System.out.println("Intersect: " + setClass.intersect(multiplicity1, multiplicity2));
        System.out.println("Subtract: " + setClass.subtract(multiplicity1, multiplicity2));
        System.out.println("SymmetricSubtract: " + setClass.symmetricSubtract(multiplicity1, multiplicity2));
    }
}
