import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 05.11.13
 * Time: 12:03
 * To change this template use File | Settings | File Templates.
 */
public class SetClassOperation implements SetOperations {
    public boolean equals(Set a, Set b) {
        return a.containsAll(b) ? true : false;

    }

    public Set union(Set a, Set b) {
        Set unionSet = new HashSet();
        for (Object n : a) unionSet.add(n);
        for (Object m : b) unionSet.add(m);
        return unionSet;
    }

    public Set subtract(Set a, Set b) {
        Set subSet = new HashSet();
        subSet.addAll(a);
        subSet.removeAll(b);
        return subSet;

    }

    public Set intersect(Set a, Set b) {
        Set intersectSet = new HashSet();
        for (Object n : a) {
            for (Object m : b) {
                if (n.equals(m)) {
                    intersectSet.add(n);
                }
            }
        }
        return intersectSet;
    }

    public Set symmetricSubtract(Set a, Set b) {
        SetClassOperation setClass = new SetClassOperation();
        Set<Integer> k1 =setClass.intersect(a, b);
        Set<Integer> k = setClass.union(a, b);
        k.removeAll(k1);
        return k;
    }
    public int rand() {
        Random r = new Random();
        int randNum = r.nextInt(10);
        return randNum;
    }
}
