import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 11.02.14
 * Time: 15:23
 * To change this template use File | Settings | File Templates.
 */
@WebServlet(name="OpenFile", urlPatterns = "/openFile")
public class OpenFile extends HttpServlet {
    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {

        // get file path
        String path = this.getServletConfig().getInitParameter("path");
        InputStream in = new FileInputStream(path);

        servletResponse.setContentType("text/plain");
        ServletOutputStream out = servletResponse.getOutputStream();

        // copy file content to output stream
        byte[] buf = new byte[4096];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }

        in.close();
        out.flush();
        out.close();
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    }
}
