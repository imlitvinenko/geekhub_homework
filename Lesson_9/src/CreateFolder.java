import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 18.02.14
 * Time: 11:42
 * To change this template use File | Settings | File Templates.
 */
@WebServlet(name="CreateFolder", urlPatterns = "/create")
public class CreateFolder extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String currentFolder=request.getParameter("directory");
        boolean flag = false;
        String nameFolder = request.getParameter("nameFolder");
        File file = new File(currentFolder+"\\"+nameFolder);
        if(nameFolder.contains(".txt")){
            flag = file.createNewFile();
            System.out.println("File: " +file.getPath() + " created ");
        } else {
            flag = file.mkdir();
            System.out.println("Directory: " +file.getPath() + " created ");
        }

        //writer.write("<a href=\"/fm?folder=" + currentFolder + "\\" + allFiles[i].getName() + "\">"+allFiles[i].getName()+"</a>");
    }
}
