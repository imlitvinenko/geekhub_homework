import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 11.02.14
 * Time: 15:23
 * To change this template use File | Settings | File Templates.
 */
@WebServlet(name="FileManager", urlPatterns = "/fm")
public class FileManager extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }


    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        String dir = "D:\\";
        String currentFolder;
        if ((currentFolder = request.getParameter("folder"))==null) {
            currentFolder = dir;
        }
        System.out.print(currentFolder);
        PrintWriter writer = response.getWriter();
        File[] allFiles = new File(currentFolder).listFiles();

                        writer.print("<html>");
                        writer.print("<head>");
                        writer.print("<title>miniMANAGER</title>");
                        writer.print("<body>");
                        writer.print("Input folder name: ");
                        writer.print("<form method=GET action=\"/create&directory=\""+currentFolder+">");
                        writer.print("<input name=\"nameFolder\" type=\"text\" size=\"40\" >"+"<br/>");
                       // writer.print("<a href=\"/create?name="+request.getParameter("nameFolder")+"&directory="+currentFolder+"\">"+"Create folder"+"</a>");
                        writer.print("<p><input type=\"submit\" value=\"Create!\"></p>");
                        writer.print("</form>");

                        //writer.print("<a href=\"#\">Створити папку</a>");
                        writer.write("<div>");
                        for (int i = 0; i < allFiles.length; i++) {
                            if (allFiles[i].isDirectory()) {
                                //currentFolder = request.getParameter("folder");
                                writer.write("<a href=\"/fm?folder=" + currentFolder + "\\" + allFiles[i].getName() + "\">"+allFiles[i].getName()+"</a>");
                                writer.write("<br/>");
                            }

                            if (allFiles[i].isFile()) {
                                //writer.write("<p>"+(allFiles[i])+"</p>");
                                writer.write("<a href=\"/openFile?folder=" + currentFolder + "\\" + allFiles[i].getName() + "\">"+allFiles[i].getName()+"</a>");
                                writer.write("<br/>");
                            }
                        }
//        for (int i = 0; i < allFiles.length; i++) {
//            writer.write("<a href=\"#\">"+allFiles[i]+"</a>");
//            writer.write("<br/>");
//        }

                        writer.write("</div>");
                        writer.print("</body>");
                        writer.print("</html>");
    }
}
