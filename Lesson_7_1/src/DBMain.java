import java.sql.SQLException;
import java.util.Scanner;

public class DBMain {

    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Enter userName:");
        String userName = sc.nextLine();
        System.out.println("Enter password");
        String password = sc.nextLine();
        System.out.println("Enter dbURL, e.g. jdbc:mysql://localhost/");
        String url = sc.nextLine();
        System.out.println("Enter dbName:");
        String dbName = sc.nextLine();
        String command = requestCommand();
        while (!"exit".equals(command)) {
            try (DBManager manager = new DBManager(url, dbName, userName, password)) {
                manager.executeQuery(command);
            } catch (SQLException e) {
                System.out.println("Error occurred: " + e.getMessage());
            }
            command = requestCommand();
        }
    }

    public static String requestCommand() {
        System.out.println("Enter valid SQL request you would like to execute, e.g. SELECT ..., CREATE ..., DELETE ..., etc. or 'exit' for exit");
        return sc.nextLine().trim();
    }
}
