import com.bethecoder.ascii_table.ASCIITable;
import dnl.utils.text.table.TextTable;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class DBManager implements AutoCloseable {

    Connection connection;

    public DBManager(String hostName, String dbName, String login, String password) throws SQLException {
        connection = DriverManager.getConnection(hostName + dbName, login, password);
    }

    @Override
    public void close() throws SQLException {
        connection.close();
    }

    public void executeQuery(String sql) throws SQLException {
        try (Statement stmt = connection.createStatement()) {
            String sqlType = sql.substring(0, 6);
            List<String> queryType = new ArrayList<String>();
            queryType.add("UPDATE");
            queryType.add("DELETE");
            queryType.add("INSERT");
            queryType.add("SELECT");
            queryType.add("CREATE");
            if (sql.isEmpty() || !queryType.contains(sqlType)) {
                System.out.println("Error!");
            } else if (!sqlType.equals("SELECT")) {
                updateExecuteUpdate(sql);
            } else {
                ResultSet result = stmt.executeQuery(sql);
                int width_size = 20;
                int width = 22;
                ResultSetMetaData resMeta = result.getMetaData();
                int columnCount = resMeta.getColumnCount();
                for (int i = 0; i < columnCount; i++) {
                    System.out.printf("%" + width_size + "s|", resMeta.getColumnName(i + 1));
                }
                System.out.print("\n");
                for (int i = 0; i < columnCount * width; i++) {
                    System.out.print("-");
                }
                System.out.print("\n");
                while (result.next()) {
                    for (int i = 0; i < columnCount; i++) {
                        Object o = result.getObject(i + 1);
                        System.out.printf("%" + width_size + "s|", o.toString());
                    }
                    System.out.print("\n");
                }

                for (int i = 0; i < columnCount * width; i++) {
                    System.out.print("-");
                }
                System.out.print("\n");

            }
        }
    }

    Statement s;

    private void updateExecuteUpdate(String command) throws SQLException {
        s = connection.createStatement();
        int num = s.executeUpdate(command);
        System.out.format("The number of modified records is %d%n", num);
    }

}
