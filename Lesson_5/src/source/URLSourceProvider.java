package source;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implementation for loading content from specified URL.<br/>
 * Valid paths to load are http://someurl.com, https://secureurl.com, ftp://frpurl.com etc.
 */
public class URLSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        Pattern urlPattern = Pattern.compile("(?:https?|ftp)://[^\\s,<>\\?]+?\\.[^\\s,<>\\?]+");
        Matcher m = urlPattern.matcher(pathToSource);
        if (m.find()) {
            return true;
        } else {
            return false;
        }

    }

    @Override
    public String load(String pathToSource) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(new URL(pathToSource).openStream()));
        String str = "";
        String tmp;
        while ((tmp = in.readLine()) != null)
            str = str + tmp;
        return str;
    }
}
