package source;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * SourceLoader should contains all implementations of SourceProviders to be able to load different sources.
 */
public class SourceLoader {
    //private List<SourceProvider> sourceProviders = new ArrayList<SourceProvider>();
    String direction;
    SourceProvider source;

//    public SourceLoader() {
//
//         source = new FileSourceProvider();
//         direction="c:/default.txt";
//    }

    public String loadSource(String pathToSource) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input sort your source(site/file)");
        String sortSource = scanner.next();
        System.out.println("Input direction:");
        if (sortSource.equals("file")) {
            source = new FileSourceProvider();
            direction = scanner.next();
        } else {
            source = new URLSourceProvider();
            direction = scanner.next();
        }

        if (source.isAllowed(direction)) {
            return source.load(direction);
        } else {
            throw new IOException();
        }
    }
}
