/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 27.10.13
 * Time: 22:47
 * To change this template use File | Settings | File Templates.
 */
public class Human implements Comparable<Human> {
    private String name;
    private double zrist;

    Human(String name, double zrist) {
        this.name = name;
        this.zrist = zrist;
    }

    Human() {
        name = "Jack";
        zrist = 1.75;
    }

    public String getName() {
        return name;
    }

    public double getZrist() {
        return zrist;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setZrist(double zrist) {
        this.zrist = zrist;
    }

    public void output() {
        System.out.println(name);
        System.out.println("" + zrist);
        System.out.println("-----------");
    }

    @Override
    public int compareTo(Human hum) {
        double zristEquel = ((Human) hum).getZrist();
        if (zrist == zristEquel)
            return 0;
        else
            return (zrist < zristEquel) ? 1 : -1;
    }


}
