import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 27.10.13
 * Time: 22:46
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    private final static int TOP = 10;
    private static Random ran = new Random();

    private static String nameRand() {
        char data = ' ';
        String dat = "";
        data = ' ';
        dat = "";
        for (int j = 1; j <= TOP; j++) {
            data = (char) (ran.nextInt(28) + 97);
            dat = data + dat;
        }
        return dat;
    }

    public static void main(String[] args) {
        Human[] hum = new Human[10];
        for (int i = 0; i < TOP; i++) {
            hum[i] = new Human(nameRand(), 1 + ran.nextDouble() % 1);
        }

        for (Human o : hum) {
            o.output();
        }

        Human[] sortHum = (Human[]) sort(hum);

        System.out.println("________Sorted_________");
        for (Human o : sortHum) {
            o.output();
        }
        System.out.println("_________________");
        for (Human o : hum) {
            o.output();
        }


    }

    public static Comparable[] sort(Comparable[] mass) {
        mass = mass.clone();
        Arrays.sort(mass);
        return mass;
    }
}