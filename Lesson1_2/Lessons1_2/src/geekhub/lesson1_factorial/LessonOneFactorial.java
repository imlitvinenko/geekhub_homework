package geekhub.lesson1_factorial;
import java.util.Scanner;
/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 10.10.13
 * Time: 17:47
 * To change this template use File | Settings | File Templates.
 */
public class LessonOneFactorial {
    static long factorial(long x){
        if(x==1) {
            return 1;
        }
        else{
            return x*factorial(x-1);
        }
    }
    public static void main(String []args){
        Scanner num  = new Scanner(System.in);
        try {
            while(true){
                System.out.print("Input numeric- ");
                long a=num.nextInt();
                if (a<0)break;
                long fact=factorial(a);
                System.out.println("Factorial is equal: "+fact);

            }
        }
        catch(Exception e){
            System.out.print("Помилочка");
        }
        num.close();

    }
}