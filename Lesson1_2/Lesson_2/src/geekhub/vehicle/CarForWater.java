package geekhub.vehicle;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 19.10.13
 * Time: 21:00
 * To change this template use File | Settings | File Templates.
 */
public abstract class CarForWater extends Vehicle {
    protected abstract void boatRun(int vol, int distance);
    public abstract int accelerate(Boolean force);
}
