package geekhub.vehicle;

/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 19.10.13
 * Time: 22:06
 * To change this template use File | Settings | File Templates.
 */
public interface TypeOfFuel {
    void refillAndMoney(int volume);
    double consumption(int distance);
}
