package geekhub.vehicle;

/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 19.10.13
 * Time: 22:03
 * To change this template use File | Settings | File Templates.
 */
public class GasTank implements TypeOfFuel {
    final int maxTankVolume = 40;
    int myTank;
    GasTank (){
        myTank=0;
    }

    public void refillAndMoney(int volume){
        myTank = volume;
        double money = volume * 1.6;
        System.out.println("Ваш бак поповнено на: "+myTank+"\nЗ вас "+money+"$");
    }
    public double consumption(int distance){
        double needGas = distance * 0.08; // З розрахунку 7 літрів на 100 км
        double difference = myTank - needGas;
        if(myTank > needGas){
            System.out.println("Бензину достатньо, в запасі: "+difference);
        }
        else{
            System.out.println("Бензину НЕ достатньо, дозаправ ще: "+(-1*difference));
        }
        return difference;
    }

}
