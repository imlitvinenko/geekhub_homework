package geekhub.vehicle;

import java.util.Random;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 19.10.13
 * Time: 21:04
 * To change this template use File | Settings | File Templates.
 */
public class Boat extends CarForWater{

    private Random random = new Random();
    private int windSpeed = Math.abs(random.nextInt())%90;

    Boat(int volume, int distance) {
        System.out.println("Ця штука по морям, річкам, та озерам плава))");
        System.out.println("Гідрометеоцентр повідомля: сила вітру- "+windSpeed);
        boatRun(volume, distance);
    }

    protected void boatRun(int vol, int distance){
        Scanner scan = new Scanner(System.in);
        TypeOfFuel tank = new GasTank();
        tank.refillAndMoney(vol);
        double remainder = tank.consumption(distance);
        boolean answer=false;
        if (remainder<0){
            System.out.println("Дозаправить СУДНО на " +remainder+ " ? (input true/false)");
            answer = Boolean.parseBoolean(scan.nextLine());
        }
        if (answer==true || remainder>=0) {
            remainder=1;
            boolean force=true;
            ForceAcceptor sail = new Sail();
            while (force==true){
                System.out.println("Все залежить від погоди, сили вітру...!!!");
                sail.run();
                System.out.println("Давай ШТУРВАЛ крути!!!");
                String rudder = scan.nextLine();
                manipulation(rudder);
            }
            if (force==false) sail.racing();
        }
        else if (answer==false && remainder<0) System.out.println("Не допливеш!!!!");
    }

    public int accelerate(Boolean force) {
        return 0;
    }

    private void manipulation(String where){
        if(where.equals("right") || where.equals("left") || where.equals("вправо") || where.equals("вліво")){
            System.out.println(turn(where));
        }
        else{
            brake();
        }
    }

    public void brake(){
        System.out.println("Якір опущено!");
    }
}
