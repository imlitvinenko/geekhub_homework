package geekhub.vehicle;

/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 19.10.13
 * Time: 20:50
 * To change this template use File | Settings | File Templates.
 */
public interface Driveable {
    int accelerate(Boolean force);
    void brake();
    String turn(String side);

}
