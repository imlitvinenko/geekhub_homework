package geekhub.vehicle;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 19.10.13
 * Time: 20:58
 * To change this template use File | Settings | File Templates.
 */
public abstract class CarForRoad extends Vehicle{

    protected void carRun(int vol, int distance){
        Scanner scan = new Scanner(System.in);
        TypeOfFuel tank = new GasTank();
        tank.refillAndMoney(vol);
        double remainder = tank.consumption(distance);
        boolean answer=false;
        if (remainder<0){
            System.out.println("Дозаправить машину на " +remainder+ " ? (input true/false)");
            answer = Boolean.parseBoolean(scan.nextLine());
        }
        if (answer==true || remainder>=0) {
            remainder=1;
            boolean force=true;

            while (force==true){
                System.out.println("Розжини тачку!!!(true-для збільшення швидкості)");
                force = Boolean.parseBoolean(scan.nextLine());
                int speed = accelerate(force);
                System.out.println("Speed - " +speed +" !!!");
                Wheel wheel = new Wheel();
                wheel.run();

                System.out.println("Давай руль крути!!!");
                String rudder = scan.nextLine();
                manipulation(rudder);
            }
        }
        else if (answer==false && remainder<0)System.out.println("Не доїдеш!!!!");
    }

    protected int speed=0;
    @Override
    public int accelerate(Boolean force){
        if(speed>=150){
        System.out.print("Не можливо розігнати більше 150 км!!!");
        }
        if(speed<150){
        speed+=20;
        }
        return speed;
    }

    private void manipulation(String where){
        if(where.equals("right") || where.equals("left") || where.equals("вправо") || where.equals("вліво")){
            System.out.println(turn(where));
        }
        else  if(where.equals("forward") || where.equals("прямо")){
            forward();
        }
        else{
            brake();
            speed=0;
        }
    }
    @Override
    public void brake(){
        System.out.println("Стоп");
    }
    protected void forward (){
        System.out.println("Вперед");
    }
}
