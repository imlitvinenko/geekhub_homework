import java.util.Scanner;
/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 19.10.13
 * Time: 20:58
 * To change this template use File | Settings | File Templates.
 */
public abstract class CarForRoad extends Vehicle {

    protected void carRun(int vol, int distance) {
        Scanner scan = new Scanner(System.in);
        TypeOfFuel tank = new GasTank();
        tank.refillAndMoney(vol);
        boolean answer = false;
        double remainder = 0;
        try {
            remainder = tank.consumption(distance);
        } catch (NotEnoughFuelException e) {
            System.out.println(e);
            StringBuilder string = new StringBuilder();
            string.append("Дозаправить машину на ");
            string.append(remainder);
            string.append(" ? (input true/false)");
            System.out.println(string);
            answer = Boolean.parseBoolean(scan.nextLine());
        }

        if (answer == true || remainder >= 0) {

            boolean force = true;
            do {
                System.out.println("Розжини тачку!!!(true-для збільшення швидкості)");
                force = Boolean.parseBoolean(scan.nextLine());
                try {
                    int speed = accelerate(force);
                } catch (BigSpeedException e) {
                    System.out.println(e.getMessage());
                }
                System.out.println("Speed - " + speed + " !!!");
                Wheel wheel = new Wheel();
                wheel.run();

                System.out.println("Давай руль крути!!!");
                String rudder = scan.nextLine();
                manipulation(rudder);
            }
            while (speed > 0);
        } else if (answer == false && remainder < 0) System.out.println("Не доїдеш!!!!");
    }

    protected int speed = 0;

    @Override
    public int accelerate(Boolean force) throws BigSpeedException {
        if (speed >= 160) {
            throw new BigSpeedException();
        }
        if (force == true && speed < 160) {
            speed += 20;
        } else if (force == false && speed >= 0) {
            speed -= 20;
        } else {
            brake();
            speed = 0;
        }

        return speed;
    }

    private void manipulation(String where) {
        if (where.equals("right") || where.equals("left") || where.equals("вправо") || where.equals("вліво")) {
            System.out.println(turn(where));
        } else if (where.equals("forward") || where.equals("прямо")) {
            forward();
        } else {
            brake();
            speed = 0;
        }
    }

    @Override
    public void brake() {
        System.out.println("Стоп");
    }

    protected void forward() {
        System.out.println("Вперед");
    }
}
