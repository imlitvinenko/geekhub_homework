import java.lang.String;

/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 19.10.13
 * Time: 20:55
 * To change this template use File | Settings | File Templates.
 */
public abstract class Vehicle implements Driveable {
    public abstract int accelerate(Boolean force) throws BigSpeedException;
    public abstract void brake();

    @Override
    public StringBuilder turn(String side) {
        StringBuilder s = new StringBuilder();
        s.append("Повернути агрегат в ");
        s.append(side);
        return s;
    }
}