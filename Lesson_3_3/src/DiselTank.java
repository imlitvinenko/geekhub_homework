/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 19.10.13
 * Time: 22:05
 * To change this template use File | Settings | File Templates.
 */

public class DiselTank implements TypeOfFuel {
    final int maxTankVolume = 50;
    int myTank;

    @Override
    public void refillAndMoney(int volume) {
        myTank = volume;
        double money = volume * 1.15;
        StringBuilder string = new StringBuilder();
        string.append("Ваш бак поповнено Солярою на: ");
        string.append(myTank);
        string.append("\nЗ вас ");
        string.append(money);
        string.append("$");
        System.out.println(string);
    }

    @Override
    public double consumption(int distance) throws NotEnoughFuelException {
        double needGas = distance * 0.07; // З розрахунку 7 літрів на 100 км
        double difference = myTank - needGas;
        if (myTank > needGas) {
            StringBuilder string = new StringBuilder();
            string.append("Пального достатньо, в запасі: ");
            string.append(difference);
            System.out.println(string);
        } else {
            throw new NotEnoughFuelException();
//            StringBuilder string = new StringBuilder();
//            string.append("Пального НЕ достатньо, дозаправ ще: ");
//            string.append((-1 * difference));
//            System.out.println(string);
        }
        return difference;
    }
}