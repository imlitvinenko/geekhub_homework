/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 19.10.13
 * Time: 21:03
 * To change this template use File | Settings | File Templates.
 */
public class PetrolCar extends CarForRoad {
    public PetrolCar(int volume, int distance)throws BigSpeedException, NotEnoughFuelException {
        System.out.println("Ця машинка на бензині їздить))");
        super.carRun(volume, distance);
    }

    @Override
    public int accelerate(Boolean force) throws BigSpeedException {
        if (speed >= 180) {
            throw new BigSpeedException();
        }
        if (force == true && speed < 150) {
            speed += 40;
        } else if (force == false && speed > 0) {
            speed -= 20;
        } else {
            brake();
            speed = 0;
        }
        return speed;
    }


}
