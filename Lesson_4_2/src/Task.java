/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 03.11.13
 * Time: 14:39
 * To change this template use File | Settings | File Templates.
 */
public class Task implements Comparable<Task> {
    private String category;
    private String description;

    public void setCategory(String category) {
        this.category = category;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }

    public void output() {
        System.out.println("Category : " + getCategory() + " Description : " + getDescription());
    }


    @Override
    public int compareTo(Task task) {
        return category.compareTo(task.category);
    }


}
