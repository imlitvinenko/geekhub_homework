import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface TaskManager {
    public void addTask();

    public void removeTask(Date date);

    //For next 3 methods tasks should be sorted by scheduled date
    public List<Task> getTasksByCategories();

    public List<Task> getTasksByCategory(String category);

    public void getTasksForDays();
}
