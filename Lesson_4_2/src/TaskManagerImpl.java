import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 03.11.13
 * Time: 14:50
 * To change this template use File | Settings | File Templates.
 */
public class TaskManagerImpl implements TaskManager{
    Map<Date, Task> tasks = new HashMap<Date, Task>();
    List<String> categoryList = new ArrayList<String>();
    Scanner scan = new Scanner(System.in);
    SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");

    public Date inputDate() {
        Date date = new Date();
        String dateStr = "15/08/2013";
        System.out.println("Input date(DD/MM/YYYY): ");
        dateStr = scan.nextLine();
        try {
            date = df.parse(dateStr);
        } catch (ParseException e) {
            System.out.println("invalid format");
        }

        if (!df.format(date).equals(dateStr)) {
            System.out.println("invalid date!!");
        } else {
            System.out.println("valid date");
        }
        return date;
    }

    public void addTask() {
        Task task = new Task();
        Date date = inputDate();
        //date=Date.parse(d);
        System.out.println("Input category: ");
        String category = scan.nextLine();

        if (!categoryList.contains(category)) {
            System.out.println("invalid category!!");
            return;
        }

        System.out.println("Input description: ");
        String descript = scan.nextLine();
        task.setCategory(category);
        task.setDescription(descript);
        tasks.put(date, task);
    }

    public void removeTask(Date date) {
        tasks.remove(date);
    }

    public void getTasksForDays() {
        for (Map.Entry<Date, Task> entry : tasks.entrySet()) {
            System.out.print("Key : " + entry.getKey() + " ");
            entry.getValue().output();
        }
    }

    public List<Task> getTasksByCategory(String category) {
        List<Task> listTask = new ArrayList<Task>();
        for (Object value : tasks.values()) {
            Task task = (Task) value;
            if (task.getCategory().equals(category)) {
                listTask.add(task);
            }
        }
        return listTask;
    }

    public List<Task> getTasksByCategories() {
        List<Task> sortCategory = new ArrayList<Task>();
        List<Task> listTask = new ArrayList<Task>(tasks.values());

        for (Task task : listTask) {
            for (String categ : categoryList) {
                if (categ.equals(task.getCategory())) {
                    sortCategory.add(task);
                }
            }
        }
        Collections.sort(sortCategory);
        return sortCategory;
    }

}
