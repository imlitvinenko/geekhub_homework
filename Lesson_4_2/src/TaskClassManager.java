import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Vana
 * Date: 05.11.13
 * Time: 12:30
 * To change this template use File | Settings | File Templates.
 */
public class TaskClassManager {
    static Scanner scan = new Scanner(System.in);
    static SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");

    public static void main(String[] args) {
        TaskManipulation task = new TaskManipulation();
        boolean exit = true;
        while (exit) {
            System.out.println("Operation:");
            System.out.println("1-add task\n2-remove task\n3-add category\n4-Tasks by category\n5-All tasks by category\n6-Tasks on ALL Days\n7-Exit");
            int operation = Integer.parseInt(scan.nextLine());
            String category;
            switch (operation) {
                case 1:
                    task.addTask();
                    break;
                case 2:
                    Date date = new Date();
                    String dateStr = "";
                    System.out.println("Input date(DD/MM/YYYY): ");
                    dateStr = scan.nextLine();
                    try {
                        date = df.parse(dateStr);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    task.removeTask(date);
                    break;
                case 3:
                    System.out.println("Input new category: ");
                    category = scan.nextLine();
                    task.categoryList.add(category);
                    break;
                case 4:
                    System.out.println("Input category: ");
                    category = scan.nextLine();
                    List<Task> listTask = task.getTasksByCategory(category);
                    for (Task o : listTask) o.output();
                    break;
                case 5:
                    List<Task> tasksByCateg = new ArrayList<Task>();
                    tasksByCateg.addAll(task.getTasksByCategories());
                    for (Task t : tasksByCateg) {
                        t.output();
                    }

                    break;
                case 6:
                    task.getTasksForDays();
                    break;
                case 7:
                    exit = false;
                    break;
            }

        }
    }
}
